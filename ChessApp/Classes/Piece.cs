﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChessApp
{
    public class Piece
    {
        public string Type;
        public string Team;
        public Piece(string Type, string Team)
        {
            this.Type = Type;
            this.Team = Team;
        }
    }
}