﻿using System.Windows.Forms;
using System.Drawing;
using System.Xml;
using System;
using System.Runtime.CompilerServices;

namespace ChessApp
{
    public class Square
    {
        public Piece Occupant = new Piece("","");
        public Button button;
        public int xCoord;
        public int yCoord;
        public int hasNotMoved;
        public delegate void ButtonClickedEventHandler(object source, EventArgs e);
        

        public Square(Panel panel, int sqaureWidth, int squareHeight, int xCoord, int yCoord, bool isLight, int hasNotMoved = 1)
        {
            this.hasNotMoved = hasNotMoved;
            this.xCoord = xCoord;
            this.yCoord = yCoord;
            button = new Button();
            button.Tag = new Point(xCoord, yCoord);
            button.Height = squareHeight;
            button.Width = sqaureWidth;
            if (isLight)
            {
                button.BackColor = Color.FromName("white");
            }
            button.Location = new Point(xCoord * button.Width , yCoord * button.Height);
            panel.Controls.Add(button);
        }

        public void newOccupant(Piece newPiece)
        {
            this.Occupant = newPiece;
            this.button.Text = newPiece.Type;

            Color whiteColor = System.Drawing.Color.FromArgb(220, 20, 207);
            Color blackColor = System.Drawing.Color.FromArgb(55, 80, 100);

            if (newPiece.Team == "White")
            {
                this.button.ForeColor = whiteColor;
            }
            else
            {
                this.button.ForeColor = blackColor;
            }

        }

        public void emptySquare()
        {
            this.Occupant.Type = "";
            this.Occupant.Team = "";
            this.button.Text = "";
        }


       
    }

}