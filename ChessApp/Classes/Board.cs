﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;


namespace ChessApp
{
    class Board
    {
        public int boardSize;
        public Panel panel;
        public Label label;
        public int squareWidth;
        public int squareHeight;
        public Square[,] boardGrid;
        Square squareSelected;
        Square destinationSquare;
        
        string nextMover = "White";
        public bool isWhiteNext = true;

        Piece Knight = new Piece("N", "White");
        Piece Empty = new Piece("", "");
        Piece Rook = new Piece("R", "White");
        Piece Pawn = new Piece("P", "White");
        Piece Bishop = new Piece("B", "White");
        Piece Queen = new Piece("Q", "White");
        Piece King = new Piece("K", "White");

        Piece bKnight = new Piece("N", "Black");
        Piece bRook = new Piece("R", "Black");
        Piece bPawn = new Piece("P", "Black");
        Piece bBishop = new Piece("B", "Black");
        Piece bQueen = new Piece("Q", "Black");
        Piece bKing = new Piece("K", "Black");





        public Board(int boardSize, Panel panel, Label label)
        {
            
            //Set variables
            this.panel = panel;
            this.label = label;
            this.boardSize = boardSize;
            this.label.Text = $"{this.nextMover}'s turn to move";
            squareHeight = panel.Height / boardSize;
            squareWidth = panel.Width / boardSize;

            //Add squares to the board:
            this.boardGrid = new Square[boardSize, boardSize];

            for (int i = 0; i < boardSize; i++)
            {
                for (int j = 0; j < boardSize; j++)
                {
                    boardGrid[i, j] = new Square(panel, squareHeight, squareWidth, i, j, (i + j) % 2 == 0);
                    boardGrid[i, j].button.Click += new EventHandler(onButtonClicked);
                    boardGrid[i, j].button.Tag = new Point(i, j);
                    boardGrid[i, j].emptySquare();
                }

            }
            initialisePieces();

        }

        //When a button is clicked, run the check move module

        void initialisePieces()
        {
            // initialise pieces

            boardGrid[0, 1].newOccupant(bPawn);
            boardGrid[1, 1].newOccupant(bPawn);
            boardGrid[2, 1].newOccupant(bPawn);
            boardGrid[3, 1].newOccupant(bPawn);
            boardGrid[4, 1].newOccupant(bPawn);
            boardGrid[5, 1].newOccupant(bPawn);
            boardGrid[6, 1].newOccupant(bPawn);
            boardGrid[7, 1].newOccupant(bPawn);

            boardGrid[0, 0].newOccupant(bRook);
            boardGrid[1, 0].newOccupant(bKnight);
            boardGrid[2, 0].newOccupant(bBishop);
            boardGrid[3, 0].newOccupant(bQueen);
            boardGrid[4, 0].newOccupant(bKing);
            boardGrid[5, 0].newOccupant(bBishop);
            boardGrid[6, 0].newOccupant(bKnight);
            boardGrid[7, 0].newOccupant(bRook);

            boardGrid[0, 6].newOccupant(Pawn);
            boardGrid[1, 6].newOccupant(Pawn);
            boardGrid[2, 6].newOccupant(Pawn);
            boardGrid[3, 6].newOccupant(Pawn);
            boardGrid[4, 6].newOccupant(Pawn);
            boardGrid[5, 6].newOccupant(Pawn);
            boardGrid[6, 6].newOccupant(Pawn);
            boardGrid[7, 6].newOccupant(Pawn);

            boardGrid[0, 7].newOccupant(Rook);
            boardGrid[1, 7].newOccupant(Knight);
            boardGrid[2, 7].newOccupant(Bishop);
            boardGrid[3, 7].newOccupant(Queen);
            boardGrid[4, 7].newOccupant(King);
            boardGrid[5, 7].newOccupant(Bishop);
            boardGrid[6, 7].newOccupant(Knight);
            boardGrid[7, 7].newOccupant(Rook);
        }

        void importXMLPosition(XmlDocument xDoc)
        {
            for (int i = 0; i < xDoc.ChildNodes.Count; i++)
            {
                Console.WriteLine(xDoc.ChildNodes[i].InnerText);
            }

        }


        void onButtonClicked(object sender, EventArgs e)
        {
            Button clickedButton = (Button)sender;
            Point Location = (Point)clickedButton.Tag;

            Console.WriteLine($"Square Coordinate is ({Location.X}, {Location.Y})");

            if (squareSelected == null)
            {

                int xCoord = Location.X; int yCoord = Location.Y;
                squareSelected = boardGrid[xCoord, yCoord];
                //if there is no piece in the square, we want to deselect it
                if (squareSelected.Occupant.Type == "")
                {
                    squareSelected = null;
                }

                Console.WriteLine($"no square was selected, picked new square. Square selected contains a {boardGrid[xCoord, yCoord].Occupant.Team} , {boardGrid[xCoord, yCoord].Occupant.Type}");
            }
            else
            {
                Console.WriteLine("square was selected, checking new move");
                int xCoord = squareSelected.xCoord; int yCoord = squareSelected.yCoord;
                int xNewCoord = Location.X; int yNewCoord = Location.Y;
                destinationSquare = boardGrid[xNewCoord, yNewCoord];
                bool movePossible = isMovePossible(xCoord, yCoord, xNewCoord, yNewCoord);
                if (movePossible && nextMover == squareSelected.Occupant.Team)
                {
                    Piece destinationPiece = new Piece(boardGrid[xNewCoord, yNewCoord].Occupant.Type, boardGrid[xNewCoord, yNewCoord].Occupant.Team);
                    boardGrid[xNewCoord, yNewCoord].newOccupant(squareSelected.Occupant);
                    boardGrid[xNewCoord, yNewCoord].hasNotMoved = 0;
                    Console.WriteLine($"new occupant for destination square is a {boardGrid[xNewCoord, yNewCoord].Occupant.Type}");
                    boardGrid[xCoord, yCoord].newOccupant(Empty);
                    
                    bool ownKingInCheck = this.isCheck(boardGrid[xNewCoord, yNewCoord].Occupant.Team);
                    if (ownKingInCheck)
                    {
                        Console.WriteLine("own king is in check! invalid move, returning pieces");
                        boardGrid[xCoord, yCoord].newOccupant(destinationSquare.Occupant);
                        boardGrid[xNewCoord, yNewCoord].newOccupant(destinationPiece);
                    }
                    else
                    {
                        isWhiteNext = !isWhiteNext;
                        nextMover = isWhiteNext ? "White" : "Black";
                        bool oponentInCheck = isCheck(nextMover);
                        string labelText = oponentInCheck ? "Check! " : "";
                        labelText += $"{this.nextMover}'s turn to move.";
                        
                        label.Text = labelText;
                    }
                }
                else
                {
                    Console.WriteLine("Invalid Move");
                }

                squareSelected = null;
                destinationSquare = null;



            }
        }

        bool isCheck(string Team)
        {
            Console.WriteLine($"Looking for checks for team {Team}");
            int kingXCoord = 0;
            int kingYCoord = 0;
            for (int i = 0; i < boardSize; i++)
            {
                for (int j = 0; j < boardSize; j++)
                {
                    if (boardGrid[i, j].Occupant.Type == "K" && boardGrid[i, j].Occupant.Team == Team)
                    {
                        kingXCoord = i;
                        kingYCoord = j;
                        Console.WriteLine($"King position is ({i}, {j})");


                        for (int x = 0; x < boardSize; x++)
                        {
                            for (int y = 0; y < boardSize; y++)
                            {
                                if (boardGrid[x, y].Occupant.Team != Team)
                                {
                                    if (isMovePossible(x, y, kingXCoord, kingYCoord))
                                    {
                                        Console.WriteLine("Check!"); return true;
                                    }
                                }

                            }
                        }

                        break;
                    }
                }

            }

 
            return false;
        }


        bool isMovePossible(int xCoord, int yCoord, int xNewCoord, int yNewCoord)
        {
            if (destinationSquare.Occupant.Team == squareSelected.Occupant.Team)
            {
                return false;
            }

            string piece = boardGrid[xCoord, yCoord].Occupant.Type;
            // Console.WriteLine($"checking possible move for piece {piece}, {xCoord}, {yCoord} moving to {xNewCoord}, {yNewCoord}");

                
            switch (piece)
            {
                case "N":
                    return isKnightMovePossible(xCoord, yCoord, xNewCoord, yNewCoord);

                case "R":
                    return isRookMovePossible(xCoord, yCoord, xNewCoord, yNewCoord);

                case "P":
                    return isPawnMovePossible(xCoord, yCoord, xNewCoord, yNewCoord);

                case "B":
                    return isBishopMovePossible(xCoord, yCoord, xNewCoord, yNewCoord);
                
                case "Q":
                    return isBishopMovePossible(xCoord, yCoord, xNewCoord, yNewCoord) ^ isRookMovePossible(xCoord, yCoord, xNewCoord, yNewCoord);
                
                case "K":
                    return isKingMovePossible(xCoord, yCoord, xNewCoord, yNewCoord);
            }

            return false;

        }


        bool isKingMovePossible(int xCoord, int yCoord, int xNewCoord, int yNewCoord)
        {
            if (boardGrid[xNewCoord, yNewCoord].Occupant.Team == boardGrid[xCoord, yCoord].Occupant.Team)
            {
                return false;
            }
            else
            {
                for (int i = -1; i <= 1; i++)
                {
                    for (int j = -1; j <= 1; j++)
                    {

                        if (xNewCoord == xCoord + i && yNewCoord == yCoord + j)
                        {
                            return true;
                        }

                    }

                }
                return false;
            }
        }

        bool isPawnMovePossible(int xCoord, int yCoord, int xNewCoord, int yNewCoord)
        {

            if (boardGrid[xCoord, yCoord].Occupant.Team == "Black")
            {
                if (boardGrid[xNewCoord, yNewCoord].Occupant.Team == "")
                {
                    if (yNewCoord == yCoord + 1 | yNewCoord == yCoord + 1 + boardGrid[xCoord, yCoord].hasNotMoved && xCoord == xNewCoord)
                    {
                        return true;
                    }
                }
                else if (boardGrid[xNewCoord, yNewCoord].Occupant.Team != boardGrid[xCoord, yCoord].Occupant.Team && ((xNewCoord == xCoord + 1) && (yNewCoord == yCoord + 1)) ^ ((xNewCoord == xCoord - 1) && (yNewCoord == yCoord + 1)))
                {
                    return true;
                }
                return false;
            }
            else
            {
                if (boardGrid[xNewCoord, yNewCoord].Occupant.Team == "")
                {
                    if (yNewCoord == yCoord - 1 | yNewCoord == yCoord - 1 - boardGrid[xCoord, yCoord].hasNotMoved && xCoord == xNewCoord)
                    {
                        return true;
                    }
                }
                else if (boardGrid[xNewCoord, yNewCoord].Occupant.Team != boardGrid[xCoord, yCoord].Occupant.Team && ((xNewCoord == xCoord + 1) && (yNewCoord == yCoord - 1)) ^ ((xNewCoord == xCoord - 1) && (yNewCoord == yCoord - 1)))
                {
                    return true;
                }
                return false;
            }

        }

        bool isKnightMovePossible(int xCoord, int yCoord, int xNewCoord, int yNewCoord)
        {
            int[,] KnightMoves = {{1,2}
                                        ,{1,-2}
                                        ,{-1,2}
                                        ,{-1,-2}
                                        ,{2,1}
                                        ,{2,-1}
                                        ,{-2,1}
                                        ,{-2,-1}};

            for (int i = 0; i < 8; i++)
            {
                if (xCoord + KnightMoves[i, 0] >= 0 && xCoord + KnightMoves[i, 0] < boardSize
                    && yCoord + KnightMoves[i, 1] >= 0 && yCoord + KnightMoves[i, 1] < boardSize)
                {
                    if (xCoord + KnightMoves[i, 0] == xNewCoord && yCoord + KnightMoves[i, 1] == yNewCoord)
                    { return true; }
                }
            }

            return false;

        }

        bool isRookMovePossible(int xCoord, int yCoord, int xNewCoord, int yNewCoord)
        {
            for (int i = 1; i < boardSize; i++)
            {
                if (xCoord + i < boardSize)
                {
                    if (boardGrid[xCoord + i, yCoord].Occupant.Team != boardGrid[xCoord, yCoord].Occupant.Team)
                    {
                        if (xCoord + i == xNewCoord && yCoord == yNewCoord) { return true; }
                        else if (boardGrid[xCoord + i, yCoord].Occupant.Team != "") { break; }
                    }
                    else { break; }
                }
                else { break; }
            }
            for (int i = 1; i < boardSize; i++)
            {
                if (xCoord - i >= 0)
                {
                    if (boardGrid[xCoord - i, yCoord].Occupant.Team != boardGrid[xCoord, yCoord].Occupant.Team)
                    {
                        if (xCoord - i == xNewCoord && yCoord == yNewCoord) { return true; }
                        else if (boardGrid[xCoord - i, yCoord].Occupant.Team != "") { break; }
                    }
                    else { break; }
                }
                else { break; }
            }
            for (int i = 1; i < boardSize; i++)
            {
                if (yCoord + i < boardSize)
                {
                    if (boardGrid[xCoord, yCoord + i].Occupant.Team != boardGrid[xCoord, yCoord].Occupant.Team)
                    {
                        if (yCoord + i == yNewCoord && xCoord == xNewCoord) { return true; }
                        else if (boardGrid[xCoord, yCoord + i].Occupant.Team != "") { break; }
                    }
                    else { break; }
                }
                else { break; }
            }
            for (int i = 1; i < boardSize; i++)
            {
                if (yCoord - i >= 0)
                {
                    if (boardGrid[xCoord, yCoord - i].Occupant.Team != boardGrid[xCoord, yCoord].Occupant.Team)
                    {
                        if (yCoord - i == yNewCoord && xCoord == xNewCoord) { return true; }
                        else if (boardGrid[xCoord, yCoord - i].Occupant.Team != "") { break; }
                    }
                    else { break; }
                }
                else { break; }
            }
            return false;
        }

        bool isBishopMovePossible(int xCoord, int yCoord, int xNewCoord, int yNewCoord)
        {
            for (int i = 1; i < boardSize; i++)
            {
                if (xCoord + i < boardSize && yCoord + i < boardSize)
                {
                    if (boardGrid[xCoord + i, yCoord + i].Occupant.Team != boardGrid[xCoord, yCoord].Occupant.Team)
                    {
                        if (xCoord + i == xNewCoord && yCoord + i == yNewCoord) { return true; }
                        else if (boardGrid[xCoord + i, yCoord + i].Occupant.Team != "") { break; }
                    }
                    else { Console.WriteLine($"piece clashes with friendly on {xCoord + i}, {yCoord + i} "); break; }
                }
                else { Console.WriteLine($"square is out of bounds at {xCoord + i}, {yCoord + i}"); break; }
            }
            for (int i = 1; i < boardSize; i++)
            {
                if (xCoord - i >= 0 && yCoord + i < boardSize)
                {
                    if (boardGrid[xCoord - i, yCoord + i].Occupant.Team != boardGrid[xCoord, yCoord].Occupant.Team)
                    {
                        if (xCoord - i == xNewCoord && yCoord + i == yNewCoord) { return true; }
                        else if (boardGrid[xCoord - i, yCoord + i].Occupant.Team != "") { break; }
                    }
                    else { break; }
                }
                else { break; }
            }
            for (int i = 1; i < boardSize; i++)
            {
                if (xCoord + i < boardSize && yCoord - i >= 0)
                {
                    if (boardGrid[xCoord + i, yCoord - i].Occupant.Team != boardGrid[xCoord, yCoord].Occupant.Team)
                    {
                        if (xCoord + i == xNewCoord && yCoord - i == yNewCoord) { return true; }
                        else if (boardGrid[xCoord + i, yCoord - i].Occupant.Team != "") { break; }
                    }
                    else { break; }
                }
                else { break; }
            }
            for (int i = 1; i < boardSize; i++)
            {
                if (xCoord - i >= 0 && yCoord - i >= 0)
                {
                    if (boardGrid[xCoord - i, yCoord - i].Occupant.Team != boardGrid[xCoord, yCoord].Occupant.Team)
                    {
                        if (yCoord - i == yNewCoord && xCoord - i == xNewCoord) { return true; }
                        else if (boardGrid[xCoord - i, yCoord - i].Occupant.Team != "") { break; }

                    }
                    else { break; }
                }
                else { break; }
            }
            return false;
        }

    }


}
